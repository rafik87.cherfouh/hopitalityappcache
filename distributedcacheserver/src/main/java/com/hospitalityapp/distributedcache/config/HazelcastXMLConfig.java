package com.hospitalityapp.distributedcache.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Properties;

@Configuration
@ComponentScan({"com.hopitalityapp.distributedcache.persistence","com.hospitalityapp.distributedcache"})
public class HazelcastXMLConfig {

    @Bean
    public HazelcastInstance hazelcastInstance(HospitalityAppSpringContext hospitalityAppSpringContext) throws IOException {

        Config config = new XmlConfigBuilder().setProperties(loadProperties()).build();
        final HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance(config);

        return hazelcastInstance;
    }

    private Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(getClass().getResourceAsStream("/application.properties"));

        return properties;
    }

}
