package com.hospitalityapp.distributedcache.config;

import com.hopitalityapp.distributedcache.persistence.dao.PotusDAO;
import com.hopitalityapp.distributedcache.persistence.repositories.CitizenRepository;
import com.hopitalityapp.distributedcache.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HospitalityAppSpringContext {

    public static PotusDAO potusDAO;

    public static CitizenRepository citizenRepository;

    public static UserRepository userRepository;

    @Autowired
    public void setPotusDAO(PotusDAO potusDAO) {
        HospitalityAppSpringContext.potusDAO = potusDAO;
    }

    @Autowired
    public void setCitizenRepository(CitizenRepository citizenRepository) {
        HospitalityAppSpringContext.citizenRepository = citizenRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        HospitalityAppSpringContext.userRepository = userRepository;
    }
}
