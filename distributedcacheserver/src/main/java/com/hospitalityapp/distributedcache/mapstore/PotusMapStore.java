package com.hospitalityapp.distributedcache.mapstore;

import com.hazelcast.core.HazelcastJsonValue;
import com.hazelcast.core.MapStore;
import com.hospitalityapp.distributedcache.config.HospitalityAppSpringContext;

import java.util.Collection;
import java.util.Map;

public class PotusMapStore implements MapStore<String, HazelcastJsonValue> {

    @Override
    public void store(String s, HazelcastJsonValue hazelcastJsonValue) {
        HospitalityAppSpringContext.potusDAO.save(s, hazelcastJsonValue);
    }

    @Override
    public void storeAll(Map<String, HazelcastJsonValue> map) {
        HospitalityAppSpringContext.potusDAO.saveAll(map);
    }

    @Override
    public void delete(String s) {

    }

    @Override
    public void deleteAll(Collection<String> collection) {

    }

    @Override
    public HazelcastJsonValue load(String s) {
        return HospitalityAppSpringContext.potusDAO.load(s);
    }

    @Override
    public Map<String, HazelcastJsonValue> loadAll(Collection<String> collection) {
        return HospitalityAppSpringContext.potusDAO.loadAll(collection);
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return HospitalityAppSpringContext.potusDAO.loadAllKeys();
    }
}
