package com.hospitalityapp.distributedcache.mapstore;

import com.hazelcast.core.MapStore;
import com.hopitalityapp.distributedcache.persistence.domain.Citizen;
import com.hospitalityapp.distributedcache.config.HospitalityAppSpringContext;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CitizenMapStore implements MapStore<String, Citizen> {

    @Override
    public void store(String s, Citizen citizen) {
        HospitalityAppSpringContext.citizenRepository.save(citizen);
    }

    @Override
    public void storeAll(Map<String, Citizen> map) {
        map.values().stream().forEach(citizen -> HospitalityAppSpringContext.citizenRepository.save(citizen));
    }

    @Override
    public void delete(String name) {
        HospitalityAppSpringContext.citizenRepository.delete(name);
    }

    @Override
    public void deleteAll(Collection<String> collection) {
        collection.stream().forEach(str -> HospitalityAppSpringContext.citizenRepository.delete(str));
    }

    @Override
    public Citizen load(String name) {
        return HospitalityAppSpringContext.citizenRepository.find(name);
    }

    @Override
    public Map<String, Citizen> loadAll(Collection<String> collection) {
        Map<String, Citizen> citizens = new HashMap<>();
        collection.stream().forEach(str -> citizens.put(str, HospitalityAppSpringContext.citizenRepository.find(str)));

        return citizens;
    }

    @Override
    public Iterable<String> loadAllKeys() {
        return HospitalityAppSpringContext.citizenRepository.findAllKeys();
    }
}
