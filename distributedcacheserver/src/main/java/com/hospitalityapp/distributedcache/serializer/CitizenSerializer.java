package com.hospitalityapp.distributedcache.serializer;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;
import com.hazelcast.nio.serialization.StreamSerializer;
import com.hopitalityapp.distributedcache.persistence.domain.Citizen;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CitizenSerializer implements StreamSerializer<Citizen> {

    // Kryo instance is not threadsafe, but expensive, so that is why it is placed in a ThreadLocal.
    private static final ThreadLocal<Kryo> KRYO_THREAD_LOCAL = new ThreadLocal<Kryo>() {
        @Override
        protected Kryo initialValue() {
            Kryo kryo = new Kryo();
            kryo.register(Citizen.class);
            return kryo;
        }
    };

    public int getTypeId() {
        return 2;
    }

    public void write(ObjectDataOutput objectDataOutput, Citizen product) throws IOException {
        Kryo kryo = KRYO_THREAD_LOCAL.get();

        Output output = new Output((OutputStream) objectDataOutput);
        kryo.writeObject(output, product);
        output.flush();
    }

    public Citizen read(ObjectDataInput objectDataInput) throws IOException {
        InputStream in = (InputStream) objectDataInput;
        Input input = new Input(in);
        Kryo kryo = KRYO_THREAD_LOCAL.get();
        return kryo.readObject(input, Citizen.class);
    }

    public void destroy() {
    }
}
