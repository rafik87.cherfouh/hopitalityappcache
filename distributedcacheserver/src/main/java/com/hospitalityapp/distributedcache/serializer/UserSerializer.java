package com.hospitalityapp.distributedcache.serializer;


import com.hopitalityapp.distributedcache.persistence.domain.User;

public class UserSerializer extends AvroSerializer<User> {

    public UserSerializer() {
        super(SerializationEnum.USER);
    }
}
