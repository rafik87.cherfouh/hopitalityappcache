package com.hopitalityapp.distributedcache.persistence.config;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.*;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static com.datastax.driver.mapping.NamingConventions.LOWER_CAMEL_CASE;
import static com.datastax.driver.mapping.NamingConventions.LOWER_SNAKE_CASE;

@Configuration
public class CassandraConfig implements DisposableBean {

    private Cluster cluster;


    @Bean
    @Qualifier("cassandraSession")
    public Session session(Cluster cluster, @Value("${spring.data.cassandra.keyspace}") String keyspace) {
        this.cluster = cluster;
        return cluster.connect(keyspace);
    }

    @Bean
    public MappingManager getManager(Session session) {
        final PropertyMapper propertyMapper =
                new DefaultPropertyMapper()
                        .setNamingStrategy(new DefaultNamingStrategy(LOWER_CAMEL_CASE, LOWER_SNAKE_CASE));
        final MappingConfiguration configuration =
                MappingConfiguration.builder().withPropertyMapper(propertyMapper).build();
        return new MappingManager(session, configuration);
    }

    public void destroy() throws Exception {
        cluster.close();
    }
}
