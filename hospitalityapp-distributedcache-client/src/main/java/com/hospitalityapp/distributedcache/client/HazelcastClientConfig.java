package com.hospitalityapp.distributedcache.client;

import com.hazelcast.client.config.ClientClasspathXmlConfig;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.YamlClientConfigBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HazelcastClientConfig {

    @Bean
    public ClientConfig clientConfig() throws Exception {
        return new ClientClasspathXmlConfig("hazelcast-client.xml");
    }
}
