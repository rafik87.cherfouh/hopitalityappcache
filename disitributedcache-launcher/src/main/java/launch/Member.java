package launch;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hopitalityapp.distributedcache.persistence.domain.Citizen;
import example.avro.User;

import java.util.Map;

public class Member {

    public static void main(String[] args) {
        HazelcastInstance hz = Hazelcast.newHazelcastInstance();
        /*Map<String, example.avro.User> map = hz.getMap("user");
        example.avro.User user = new User();
        user.setName("rafik");
        user.setFavoriteColor("blue");
        user.setFavoriteNumber(1);

        map.put("rafik", user);

        User rafik = map.get("rafik");
        System.out.println(rafik);*/

        IMap<String, Citizen> citizenIMap = hz.getMap("citizen");
        Citizen citizen = new Citizen("abdellah", 11, "yellow");
        citizenIMap.put("abdellah", citizen);

        Citizen myCitizen = citizenIMap.get("abdellah");
        System.out.println(myCitizen.toString() + "\n");

        Hazelcast.shutdownAll();
    }
}